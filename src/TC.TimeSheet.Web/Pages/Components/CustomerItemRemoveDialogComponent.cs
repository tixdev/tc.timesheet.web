using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using TC.TimeSheet.Web.Data.EF;
using TC.TimeSheet.Web.Models;

namespace TC.TimeSheet.Web.Pages.Components
{
    public class CustomerItemRemoveDialogComponent : ComponentBase
    {
        [Inject] 
        public IJSRuntime JsRuntime { get; set; }
        
        [Inject] 
        public TimesheetEntities Entities { get; set; }
        
        [Parameter]
        public EventCallback<string> OnCustomerItemRemoved { get; set; }

        public CustomerItemRemoveDialogModel _customerItemRemoveDialogModel = new CustomerItemRemoveDialogModel();
    
        protected override async Task OnInitializedAsync()
        {

        }

        protected async void Delete()
        {
            var customer = Entities.Customers.Find(Guid.Parse(_customerItemRemoveDialogModel.Id));

            customer.Deleted = true;
            
            Entities.SaveChanges();

            await OnCustomerItemRemoved.InvokeAsync(customer.Id.ToString());

            await JsRuntime.InvokeAsync<bool>("closeDialog", "modal-remove-customer-item");

            await JsRuntime.InvokeAsync<bool>("toastSuccess",
                new {Type = "success", Title = $"Cliente {customer.Name} rimosso"});
        }

        public async Task Show(string customerId)
        {
            var customer = Entities.Customers.Find(Guid.Parse(customerId));

            _customerItemRemoveDialogModel = new CustomerItemRemoveDialogModel
            {
                Id = customer.Id.ToString(),
                Name = customer.Name
            };
        
            await JsRuntime.InvokeAsync<object>("showDialog", "modal-remove-customer-item");
        }
    }
}