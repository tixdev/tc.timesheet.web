namespace TC.TimeSheet.Web.Pages.Components
{
    public class CustomerItemRemoveDialogModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}