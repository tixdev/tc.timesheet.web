using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using TC.TimeSheet.Web.Data.EF;
using TC.TimeSheet.Web.Models;

namespace TC.TimeSheet.Web.Pages
{
    public class IndexPage : ComponentBase
    {
        [Inject] 
        public TimesheetEntities TimesheetEntities { get; set; }
        
        public readonly List<TimesheetReadModel> TimesheetReadModels = new List<TimesheetReadModel>();

        protected override async void OnInitialized()
        {
            await LoadTimesheets();
        }

        private async Task LoadTimesheets()
        {
            TimesheetReadModels.Clear();
            var timesheets = await TimesheetEntities.TimesheetItems.Include(t => t.Customer)
                .OrderByDescending(t => t.Date).Take(10).ToListAsync();

            timesheets.ForEach(c =>
            {
                var model = new TimesheetReadModel
                {
                    Id = c.Id.ToString(),
                    Hours = TimeSpan.FromHours((double)c.Hours).ToString(@"hh\:mm"),
                    CustomerName = c.Customer.Name,
                    FromTo = DateTime.ParseExact(c.Date, "yyyyMMdd", new CultureInfo("it-IT")).ToLongDateString()
                };

                TimesheetReadModels.Add(model);
            });

            StateHasChanged();
        }
    }
}