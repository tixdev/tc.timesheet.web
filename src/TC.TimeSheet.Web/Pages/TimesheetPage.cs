using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.JSInterop;
using TC.TimeSheet.Web.Data.EF;
using TC.TimeSheet.Web.Extensions;
using TC.TimeSheet.Web.Models;
using TC.TimeSheet.Web.Pages.Components;

namespace TC.TimeSheet.Web.Pages
{
    public class TimesheetPage : ComponentBase
    {
        [Inject] public IJSRuntime JsRuntime { get; set; }

        [Inject] public TimesheetEntities TimesheetEntities { get; set; }

        internal TimesheetCreate _ctrlTimesheetCreate;
        internal TimesheetItemRemove _ctrlTimesheetItemRemove;

        internal readonly List<TimesheetReadModel> _timesheetReadModels = new List<TimesheetReadModel>();

        protected override async void OnInitialized()
        {
            await JsRuntime.InvokeAsync<object>("initDateTimeRangePicker");

            _selectedYearMonthId = DateTime.Now.ToString("yyyyMM");

            await LoadTimesheets();
        }

        internal async void OnTimesheetCreated(string s)
        {
            await LoadTimesheets();
        }

        internal async void OnTimesheetItemRemoved(string s)
        {
            await LoadTimesheets();
        }

        private string _selectedYearMonthId;

        protected string SelectedYearMonthId
        {
            get => _selectedYearMonthId;
            set
            {
                _selectedYearMonthId = value;

                LoadTimesheets();
            }
        }

        private async Task LoadTimesheets()
        {
            _timesheetReadModels.Clear();

            var timesheets = await TimesheetEntities.TimesheetItems.Include(t => t.Customer).IgnoreQueryFilters()
                .Where(t => t.Date.StartsWith(_selectedYearMonthId)).ToListAsync();

            timesheets = timesheets.OrderByDescending(t => t.Date).Take(10).ToList();

            timesheets.ForEach(timesheetItem =>
            {
                var model = new TimesheetReadModel
                {
                    Id = timesheetItem.Id.ToString(),
                    Paid = timesheetItem.Paid,
                    Hours = TimeSpan.FromHours((double) timesheetItem.Hours).ToString(@"hh\:mm"),
                    CustomerName = timesheetItem.Customer.Name,
                    FromTo = DateTime.ParseExact(timesheetItem.Date, "yyyyMMdd", new CultureInfo("it-IT"))
                        .ToLongDateString()
                };

                _timesheetReadModels.Add(model);
            });

            StateHasChanged();
        }

        internal List<YearMonthModel> YearMonthModels()
        {
            var models = new List<YearMonthModel>
            {
                new YearMonthModel(DateTime.Now)
            };
            
            for (var i = 2; i < 13; i++)
            {
                var date = DateTime.Now.AddMonths((i - 1) * -1);
                
                if(TimesheetEntities.TimesheetItems.Any(t => t.Date.StartsWith($"{date.Year}{date.Month:00}")))
                    models.Add(new YearMonthModel(date));
            }

            return models;
        }

        internal async Task CreateTimesheet()
        {
            await _ctrlTimesheetCreate.OnShown();
        }

        protected async Task RemoveTimesheetItem(string timesheetItemId)
        {
            await _ctrlTimesheetItemRemove.OnShown(timesheetItemId);

            await JsRuntime.InvokeAsync<object>("showDialog", "modal-remove-timesheet-item");
        }
    }
}