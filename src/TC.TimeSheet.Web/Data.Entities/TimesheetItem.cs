using System;
using System.ComponentModel.DataAnnotations;

namespace TC.TimeSheet.Web.Data.Entities
{
    public class TimesheetItem
    {
        public TimesheetItem()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public Customer Customer { get; set; }

        [Required] 
        public string Date { get; set; }
        
        [Required]
        public decimal Hours { get; set; }

        public bool Paid { get; set; }
    }
}