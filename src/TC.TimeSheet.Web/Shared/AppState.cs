using System;

namespace TC.TimeSheet.Web.Shared
{
    public class TimesheetState
    {
        public event Action OnCreateTimesheet;

        public void CreateTimesheet()
        {
            NotifyStateChanged();
        }

        private void NotifyStateChanged() => OnCreateTimesheet?.Invoke();
    }
}