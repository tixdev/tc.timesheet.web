﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TC.TimeSheet.Web.Migrations
{
    public partial class Added_Timesheet_Paid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Paid",
                table: "TimesheetItems",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Paid",
                table: "TimesheetItems");
        }
    }
}
