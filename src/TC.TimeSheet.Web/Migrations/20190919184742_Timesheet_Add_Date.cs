﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TC.TimeSheet.Web.Migrations
{
    public partial class Timesheet_Add_Date : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Date",
                table: "Timesheets",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "Timesheets");
        }
    }
}
