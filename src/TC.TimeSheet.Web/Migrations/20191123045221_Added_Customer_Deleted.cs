﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TC.TimeSheet.Web.Migrations
{
    public partial class Added_Customer_Deleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Customers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Customers");
        }
    }
}
