namespace TC.TimeSheet.Web.Models
{
    public class CustomerCreateModel
    {
        public string Name { get; set; }

        public int? AmountPerHour { get; set; }
    }
}