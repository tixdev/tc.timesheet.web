using System;

namespace TC.TimeSheet.Web.Models
{
    public class TimesheetReadModel
    {
        public string Id { get; set; }

        public string CustomerName { get; set; }

        public string FromTo { get; set; }

        public string Hours { get; set; }

        public bool Paid { get; set; }
    }
}