namespace TC.TimeSheet.Web.Models
{
    public class TimesheetDeleteModel
    {
        public string Id { get; set; }

        public string CustomerName { get; set; }

        public string Date { get; set; }

        public string Hours { get; set; }

        public string Minutes { get; set; }
        
    }
}