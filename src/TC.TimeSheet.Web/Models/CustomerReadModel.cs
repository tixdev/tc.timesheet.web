namespace TC.TimeSheet.Web.Models
{
    public class CustomerReadModel
    {
        public string Id { get; set; }
        
        public string Name { get; set; }

        public int AmountPerHour { get; set; }
    }
}